const router = require("express").Router();

router.get("/", (req, res) => {
  res.json({ msg: "Select all posts." });
});

router.post("/", (req, res) => {
  res.json(req.body);
});

router
  .route("/:id")
  .get((req, res) => res.json({ msg: "Select post id is " + req.params.id }))
  .patch((req, res) => res.json({ msg: "Update post id is " + req.params.id }))
  .delete((req, res) =>
    res.json({ msg: "Delete post id is " + req.params.id })
  );

// router.get("/:id", (req, res) => {
//     let id = req.params.id;
//     res.json({ msg: "Select post id is " + id });
//   });

// router.patch("/:id", (req, res) => {
//   let id = req.params.id;
//   res.json({ msg: "Update post id is " + id });
// });

// router.delete("/:id", (req, res) => {
//   let id = req.params.id;
//   res.json({ msg: "Delete post id is " + id });
// });

module.exports = router;
