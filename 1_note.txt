const express = require("express");
const app = express();

//get, post, put, patch, delete (CRUD)
//http://192.168.99.175:3000/ <=> http://localhost:3000/

app.get("/dd", () => {
  console.log("We are here at / route");
});

app.get("/user", (req, res, next) => {
  res.status(200).json({ name: "Kay", age: 23, subject: "NodeJS" });
});

app.post("/user", (req, res, next) => {
  res.status(200).json({ msg: "Register Success" });
});

app.patch("/users/:id/:name", (req, res) => {
  let id = req.params.id;
  let name = req.params.name;
  res.status(200).json({ id, name });
});

app.delete("/users/:id", (req, res) => {
  let id = req.params.id;
  res.status(200).json({ msg: "Delete id is " + id });
});

app.get("*", (req, res, next) => {
  res.json({ msg: "No Route Found!" });
});

app.listen(3000, console.log("Server is running at port 3000"));



........................................................................

let users = [
  { id: 1, name: "Mg Mg", age: 19 },
  { id: 2, name: "Ag Ag", age: 19 },
  { id: 3, name: "Tun Tun", age: 19 },
];

router.get("/", (req, res) => {
  res.json(users);
});
ddd
router.get("/:id", (req, res) => {
  let id = req.params.id;
  let user = users.find((usr) => usr.id == id);
  if (user) {
    res.json(user);
  } else {
    res.json({ msg: "No user  this id sir!" });
  }
});

router.post("/users", (req, res) => {
  let id = req.body.id;
  let name = req.body.name;
  let age = req.body.age;
  let newUser = {
    id: id,
    name: name,
    age: age,
  };

  users.push(newUser);
  res.json({ users });
});

router.patch("/:id", (req, res) => {
  let id = req.params.id;
  let editUser = users.find((usr) => usr.id == id);
  if (editUser) {
    editUser.name = req.body.name;
    res.json({ users });
  } else {
    res.json({ msg: "User with this id is not found!" });
  }
});

router.delete("/:id", (req, res) => {
  let id = req.params.id;
  users = users.filter((usr) => usr.id != id);
  res.json({ users });
});

router.get("*", (req, res) => {
  res.json({ msg: "No route found!" });
});

